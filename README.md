# LocalLaunch
Small API running on a local Windows WSL, takes commands from remote hosts, and launches local jobs on my Windows computer.

This enables me to open links in new tabs in my browser directly while working on the command line on remote hosts. I can
for instance work in a git projekt, and then quickly open that project on our Gitlab or Jenkins server in my local browser.

The API only takes specific commands, so you cannot run arbitrary commands and you need to specify an access token to be allowed
to run commands.

# Available commands

## /opentab?link=\<link\>
Open a link in a new tab in my browser. The link must be a "valid" link and are checked with a regex, which will not allow strange
stuff. So a lot of valid links will be rejected, but I only use it to open links, which are very well formated.

## /openimage?wnr=\<nnnnn\>
Open an image of a specific employee from our network drive
